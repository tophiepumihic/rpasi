class Student < ApplicationRecord

	include PgSearch
  include Filterable
  pg_search_scope :search_by_name, :against => [:full_name, :lr_number]
  scope :by_course, -> (course_id) { where course_id: course_id }
  scope :by_track, -> (track_id) { where track_id: track_id }
  scope :by_year_level, -> (year_level_id) { where year_level_id: year_level_id }
  
	belongs_to :course
	belongs_to :track
	belongs_to :year_level
	has_one :address, dependent: :destroy
  has_many :subscriptions
  has_many :payments
	has_attached_file :profile_photo,
                    styles: { large: "120x120>",
                   medium: "70x70>",
                    thumb: "40x40>",
                    small: "30x30>",
                    x_small: "20x20>"},
                      default_url: ":style/profile_default.jpg",
                    :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
                    :url => "/system/:attachment/:id/:style/:filename"
  validates_attachment :profile_photo, content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }

	enum gender: [:male, :female]

  accepts_nested_attributes_for :address

	validates :lr_number, :first_name, :last_name, :middle_name, :gender, :course_id, presence: true
  validates :lr_number, uniqueness: true
  
  before_save :set_full_name, :set_track

  delegate :details, to: :address, prefix: true, allow_nil: true
  
  def fullname
    "#{first_name} #{middle_name.first.capitalize}. #{last_name}"
  end

  def reversed_name
  	"#{last_name}, #{first_name} #{middle_name.first.capitalize}."
  end

  def strand
    if course.track.abbr.blank?
      "#{course}"
    else
      "#{course.track.abbr} - #{course}"
    end
  end

  def tuition_fee
    Fee.where(:miscelaneous => false).sum(:amount)
  end

  def miscelaneous_fee
    Fee.where(:miscelaneous => true).sum(:amount)
  end

  def total_fee
    tuition_fee + (miscelaneous_fee * 2)
  end

  def current_subscription
    subscriptions.last
  end

  def previous_subscription
    subscription.first
  end

  def current_subscription_amount
    if subscriptions.present?
      subscriptions.last.amount
    else
      0
    end
  end

  def current_subscription_payment_total
    if current_subscription.present?
      current_subscription.payments.sum(:amount)
    else
      0
    end
  end

  def current_balance
    current_subscription_amount - current_subscription_payment_total
  end

  def subscription_paid?
    current_balance == 0
  end

  def status
    if subscription_paid?
      "Paid"
    end
  end

  def last_payment_created_at
    current_subscription.payments.last.created_at
  end

  def set_year_level!
    grade_11 = YearLevel.find_by(name: "Grade 11")
    grade_12 = YearLevel.find_by(name: "Grade 12")
    if subscriptions.count == 1
      self.update(year_level_id: grade_11.id)
    else
      self.update(year_level_id: grade_12.id)
    end
  end

  def subscribe!
    Subscription.create!(name: "SY #{Time.now.year}-#{Time.now.next_year.year}",
      amount: total_fee, student_id: self.id)
    set_year_level!
  end

  private
  def set_full_name
    self.full_name = fullname
  end

  def set_track
    course = Course.find(course_id)
    self.track_id = course.track.id
  end
end
