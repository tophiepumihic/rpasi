class Fee < ApplicationRecord

	def self.misc
		all.where(miscelaneous: true)
	end

	def self.tuition
		all.where(miscelaneous: false)
	end
end
