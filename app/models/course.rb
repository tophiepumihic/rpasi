class Course < ApplicationRecord
  belongs_to :track
  has_many :students

  validates :name, :track_id, presence: true

  def to_s
  	name
  end
end
