class Payment < ApplicationRecord
  belongs_to :subscription
  belongs_to :student
  validates_numericality_of :amount, :less_than_or_equal_to => :current_balance, message: 'must be less than or equal to the remaining balance.'

  def current_balance
  	student.current_balance
  end

end
