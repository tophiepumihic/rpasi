class Track < ApplicationRecord
	has_many :courses
	has_many :students

	def to_s
		name
	end
end
