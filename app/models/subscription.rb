class Subscription < ApplicationRecord
  belongs_to :student
  has_many :payments
end
