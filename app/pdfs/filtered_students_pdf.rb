class FilteredStudentsPdf < Prawn::Document
  TABLE_WIDTHS = [300, 272]
  TABLE_COLUMNS = [200, 100, 100, 172]
  def initialize(students, track, course, year_level, balance_status, view_context)
    super(margin: 20, page_size: [612, 1008], page_layout: :portrait)
    @students = students
    @track_id = track if track.present?
    @course_id = course if course.present?
    @year_level_id = year_level if year_level.present?
    @track = Track.find(track) if track.present?
    @course = Course.find(course) if course.present?
    @year_level = YearLevel.find(year_level) if year_level.present?
    @balance_status = balance_status
    @view_context = view_context
    heading
    students_table
  end
  def price(number)
    @view_context.number_to_currency(number, :unit => "P ")
  end

  def filtered_course
    Course.order(:name).select {|c| c.students.filter(by_track: @track_id, by_course: @course_id, by_year_level: @year_level_id).present?}
  end

  def filtered_year_level
    YearLevel.order(:name).select {|y| y.students.filter(by_track: @track_id, by_course: @course_id, by_year_level: @year_level_id).present?}
  end

  def status
    if @balance_status == 'w/ balance'
      "with balance"
    else
      "with no balance"
    end
  end

  def title
    if @balance_status.blank?
      "LIST OF STUDENTS"
    else
      if @balance_status == "w/ balance"
        "LIST OF STUDENTS #{status.upcase}"
      else
        "LIST OF STUDENTS #{status.upcase}"
      end
    end
  end

  def heading
    text "RIVERVIEW POLYTECHNIC AND ACADEMIC SCHOOL, INC.", align: :center, size: 11, style: :bold
    text "Kiangan, Ifugao", align: :center, size: 10
    move_down 10
    text title, size: 11, align: :center, style: :bold
    move_down 2
    text Subscription.last.name, size: 9, align: :center
    move_down 2
    text "As of #{Time.zone.now.strftime('%B %e, %Y')}", size: 9, align: :center
    move_down 4
    stroke_horizontal_rule
    move_down 10
  end

  def students_table
    if @students.blank?
      move_down 10
      text "No Records found.", align: :center
    else
      move_down 10
      if @balance_status.present?
        if @balance_status == "w/o balance"
          name_only
        else #with balance
          name_balance
        end
      else
        name_balance
      end
    end
  end

  def name_only
    header = ["NAME"]
    footer = [""]
    filtered_course.each do |course|
      filtered_year_level.each do |year_level|
        text "#{course.name} - #{year_level.name}", size: 9, align: :center, style: :bold
        students_data = Student.where(course_id: course.id).where(year_level_id: year_level.id).map { |e| [e.reversed_name]}
        table_data = [header, *students_data, footer]
        table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: [572]) do
          cells.borders = [:top]
          row(0).font_style = :bold
          column(2).align = :right
        end
        move_down 20
      end
    end
  end

  def name_balance
    header = ["NAME", "", "BALANCE"]
    footer = ["", "", ""]
    filtered_course.each do |course|
      filtered_year_level.each do |year_level|
        text "#{course.name} - #{year_level.name}", size: 9, align: :center, style: :bold
        students_data = Student.where(course_id: course.id).where(year_level_id: year_level.id).map { |e| [e.reversed_name, "", price(e.current_balance)]}
        table_data = [header, *students_data, footer]
        table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: [200, 172, 200]) do
          cells.borders = [:top]
          row(0).font_style = :bold
          column(2).align = :right
        end
        move_down 20
      end
    end
  end

  # def name_course_year_level_balance
  #   header = [["NAME", "COURSE", "GRADE", "BALANCE"]]
  #   table(header, :cell_style => {size: 9, :padding => [2, 4, 2, 4]}, column_widths: TABLE_COLUMNS) do
  #     cells.borders = []
  #     row(0).font_style = :bold
  #     column(3).align = :right
  #   end
  #   stroke_horizontal_rule
  #   header = ["", "", "", ""]
  #   footer = ["", "", "", ""]
  #   students_data = @students.map { |e| [e.reversed_name, e.course.name, e.year_level.name, price(e.current_balance)]}
  #   table_data = [header, *students_data, footer]
  #   table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_COLUMNS) do
  #     cells.borders = [:top]
  #     column(3).align = :right
  #   end
  # end
end
