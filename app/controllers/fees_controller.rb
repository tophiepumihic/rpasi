class FeesController < ApplicationController

	def new
		@fee = Fee.new
	end

	def create
		@fee = Fee.create(fee_params)
	end

	def edit
    @fee = Fee.find(params[:id])
  end

  def update
    @fee = Fee.find(params[:id])
    @fee.update(fee_params)
  end

  def destroy
  	@fee = Fee.find(params[:id])
  	@fee.destroy
  	redirect_to settings_url, notice: 'Fee deleted!'
  end

  private
  def fee_params
    params.require(:fee).permit(:name, :amount, :miscelaneous)
  end

end
