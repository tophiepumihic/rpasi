module Students
  class PaymentsController < ApplicationController

    def new
      @student = Student.find(params[:student_id])
      @payment = @student.payments.build
    end

    def create
      @student = Student.find(params[:student_id])
      @payment = @student.payments.create(payment_params)
    end

    def edit
      @student = Student.find(params[:student_id])
      @payment = Payment.find(params[:id])
    end

    def update
      @student = Student.find(params[:student_id])
      @payment.update(track_params)
    end

    def destroy
      @payment = Track.find(params[:id])
      @payment.destroy
      redirect_to settings_url, notice: 'Track deleted!'
    end

    private
    def payment_params
      params.require(:payment).permit(:amount, :subscription_id, :student_id)
    end

  end
end