class StudentsController < ApplicationController
	autocomplete :student, :full_name, full: true

	def autocomplete
    @students = Student.all
    @names = @students.map { |m| m.full_name }
    render json: @names
  end

	def index
		@tracks = Track.all
		@courses = Course.all
		@year_levels = YearLevel.all
		@track = params[:track_id] if params[:track_id].present?
		@course = params[:course_id] if params[:course_id].present?
		@year_level = params[:year_level_id] if params[:year_level_id].present?
		@balance_status = params[:balance_status]
		@full_name = params[:full_name]

		if @full_name.present?
      @filtered = Student.order(:last_name).search_by_name(params[:full_name])
      @students = Kaminari.paginate_array(@filtered).page(params[:page]).per(30)
    else
    	respond_to do |format|
    		format.html do
    			if @balance_status.present?
    				if @balance_status == "w/ balance"
				    	@filtered = Student.filter(by_track: @track, by_course: @course, by_year_level: @year_level).select{|s| s.subscription_paid? == false}
				    	@students = Kaminari.paginate_array(@filtered).page(params[:page]).per(30)
				    else
				    	@filtered = Student.filter(by_track: @track, by_course: @course, by_year_level: @year_level).select{|s| s.subscription_paid? == true}
				    	@students = Kaminari.paginate_array(@filtered).page(params[:page]).per(30)
				    end
			    else
			    	@filtered = Student.filter(by_track: @track, by_course: @course, by_year_level: @year_level)
				    @students = Kaminari.paginate_array(@filtered).page(params[:page]).per(30)
			    	#@students = Student.where(nil)
			    	#filtering_params(params).each do |value|
			    	#	@students = @students.public_send(value) if value.present?
			    	#end
			    end
		    end
		  end
    end
	end

	def report
		@track = params[:track_id] if params[:track_id].present?
		@course = params[:course_id] if params[:course_id].present?
		@year_level = params[:year_level_id] if params[:year_level_id].present?
		@balance_status = params[:balance_status]
		if @balance_status.present?
			if @balance_status == "w/ balance"
	    	@students = Student.filter(by_track: @track, by_course: @course, by_year_level: @year_level).select{|s| s.subscription_paid? == false}
	    else
	    	@students = Student.filter(by_track: @track, by_course: @course, by_year_level: @year_level).select{|s| s.subscription_paid? == true}
	    end
    else
    	@students = Student.filter(by_track: @track, by_course: @course, by_year_level: @year_level)
    end
    respond_to do |format|
  		format.html
			format.pdf do
				pdf = FilteredStudentsPdf.new(@students, @track, @course, @year_level, @balance_status, view_context)
    		send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Students.pdf"
	    end
	  end
	end

	def new
		@student = Student.new
		@student.build_address
	end

	def create
		@fees = Fee.all
		if @fees.present?
			@student = Student.create(student_params)
    	if @student.save
				@student.subscribe!
				redirect_to payments_student_path(@student), notice: 'Student record has been saved.'
			else
				render :new
			end
		else
			render :new, notice: 'School Fees are not yet set.'
		end
	end

	def edit
		@student = Student.find(params[:id])
	end

	def update
		@student = Student.find(params[:id])
		@student.update_attributes(student_params)
		if @student.save
			redirect_to info_student_path(@student), notice: 'Student record has been updated!'
		else
			render :edit
		end
	end

	def info
		@student = Student.find(params[:id])
	end

	def payments
		@student = Student.find(params[:id])
		@payments = @student.current_subscription.payments
	end

	def options
		@student = Student.find(params[:id])
	end

	def enroll
		@student = Student.find(params[:student_id])
		@student.subscribe!
		redirect_to payments_student_path(@student), notice: 'Enrollment done.'
	end

	def destroy
		@student = Student.find(params[:id])
	end

	private
  def student_params
    params.require(:student).permit(
    	:last_name, 
    	:first_name, 
    	:middle_name, 
    	:gender, 
    	:profile_photo, 
    	:course_id,
    	:track_id,
    	:year_level_id,
    	:lr_number,
    	address_attributes:[:id, :house_number, :street, :barangay, :municipality, :province, :id ])
  end

  def filtering_params(params)
	  params.slice(:track_id, :course_id, :year_level_id, :balance_status)
	end
end