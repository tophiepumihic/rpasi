class CoursesController < ApplicationController

	def new
		@course = Course.new
	end

	def create
		@course = Course.create(course_params)
	end

	def edit
    @course = Course.find(params[:id])
  end

  def update
    @course = Course.find(params[:id])
    @course.update(course_params)
  end

  def destroy
  	@course = Course.find(params[:id])
  	@course.destroy
  	redirect_to settings_url, notice: 'Course deleted!'
  end

  private
  def course_params
    params.require(:course).permit(:name, :track_id)
  end

end
