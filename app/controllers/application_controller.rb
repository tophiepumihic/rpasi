class ApplicationController < ActionController::Base
	include Pundit
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  rescue_from Pundit::NotAuthorizedError, with: :permission_denied

  def after_sign_in_path_for(current_user)
    if current_user.is_a?(Employee) && current_user.developer?
      students_url
    elsif current_user.is_a?(Employee) && current_user.admin?
      students_url
    elsif current_user.is_a?(Employee) && current_user.cashier?
      students_url
    end
  end

  def permission_denied
    redirect_to  after_sign_in_path_for(current_user), alert: "We're sorry but you are not allowed to access this page or feature."
  end
end
