class SettingsController < ApplicationController

	def index
		@tracks = Track.order(:name).all
		@tuition_fees = Fee.tuition.order(:name).all
		@miscellaneous_fees = Fee.misc.order(:name).all
	end
end