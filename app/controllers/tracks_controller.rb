class TracksController < ApplicationController

	def new
		@track = Track.new
	end

	def create
		@track = Track.create(track_params)
	end

	def edit
    @track = Track.find(params[:id])
  end

  def update
    @track = Track.find(params[:id])
    @track.update(track_params)
  end

  def destroy
  	@track = Track.find(params[:id])
  	@track.destroy
  	redirect_to settings_url, notice: 'Track deleted!'
  end

  private
  def track_params
    params.require(:track).permit(:name)
  end

end
