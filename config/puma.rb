#!/usr/bin/env puma
environment ENV['RAILS_ENV'] || 'production'
daemonize true
pidfile "/var/www/rpasi/shared/tmp/pids/puma.pid"
stdout_redirect "/var/www/rpasi/shared/tmp/log/stdout", "/var/www/rpasi/shared/tmp/log/stderr"
threads 0, 16
bind "unix:///var/www/rpasi/shared/tmp/sockets/puma.sock"
