Rails.application.routes.draw do
  devise_for :users, :controllers => { :registrations => "users", sessions: "users/sessions" }
  get 'students/index'

  root :to => 'students#index', :constraints => lambda { |request| request.env['warden'].user.nil? }, as: :unauthenticated_root
  root :to => 'students#index', :constraints => lambda { |request| request.env['warden'].user.role == ('admin' || 'cashier' || 'developer') if request.env['warden'].user }, as: :admin_root

  resources :students do
  	get :autocomplete_student_full_name, on: :collection
    resources :payments, only: [:new, :create, :edit, :update], module: :students
    match "/info" => "students#info", as: :info, via: [:get], on: :member
    match "/payments" => "students#payments", as: :payments, via: [:get], on: :member
    match "/options" => "students#options", as: :options, via: [:get], on: :member
    match "/enroll" => "students#enroll", as: :enroll, via: [:get, :post]
    match "/report" => "students#report", via: [:get], on: :collection
  end
  resources :users, only: [:show, :edit, :update] do 
    match "/info" => "users#info", as: :info, via: [:get], on: :member
    match "/activities" => "users#activities", as: :activities, via: [:get], on: :member
  end
  resources :settings
  resources :courses
  resources :fees

end
