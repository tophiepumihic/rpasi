class CreateSubscriptions < ActiveRecord::Migration[5.1]
  def change
    create_table :subscriptions do |t|
      t.string :name
      t.decimal :amount
      t.belongs_to :student, index: true, foreign_key: true

      t.timestamps
    end
  end
end
