class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.decimal :amount
      t.belongs_to :subscription, index: true, foreign_key: true
      t.belongs_to :student, index: true, foreign_key: true

      t.timestamps
    end
  end
end
