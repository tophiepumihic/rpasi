class AddProfilePhotoToStudent < ActiveRecord::Migration[5.1]
  def up
    add_attachment :students, :profile_photo
  end

  def down
    remove_attachment :students, :profile_photo
  end
end
