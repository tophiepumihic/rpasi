class CreateFees < ActiveRecord::Migration[5.1]
  def change
    create_table :fees do |t|
      t.string :name
      t.decimal :amount
      t.boolean :miscelaneous, default: false

      t.timestamps
    end
  end
end
