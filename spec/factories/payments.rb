FactoryBot.define do
  factory :payment do
    subscription nil
    student nil
    amount "9.99"
  end
end
